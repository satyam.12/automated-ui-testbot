from time import sleep
from random import randint
from cv2 import imread, cvtColor, COLOR_BGR2GRAY
from skimage.metrics import structural_similarity
from configparser import ConfigParser
from gtts import gTTS
from playsound import playsound
import pyautogui as auto
import speech_recognition as sr
import string
import datetime
import os




config = ConfigParser()
config.read('config.ini')


def greet_me():
    hour = int(datetime.datetime.now().hour)
    if hour>=0 and hour<12:
        speak("Good Morning!")

    elif hour>=12 and hour<18:
        speak("Good Afternoon!")

    else:
        speak("Good Evening!")

    speak("I am Automated UI testbot, prepairing for the test")

def take_command():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening...")
        r.pause_threshold = 1
        playsound(config['sound']['robo2'])
        audio = r.listen(source)

    try:
        print("Recognizing...")
        playsound(config['sound']['robo'])
        query = r.recognize_google(audio, language='en-in')
        print(f"User said: {query}\n")

    except Exception:
        print("Say that again please...")
        return "None"
    return query


def speak(text):
    file = gTTS(text=text,lang='en')
    file.save(config['tts']['path_n_name'])
    playsound(config['tts']['path_n_name'])
    os.remove(config['tts']['path_n_name'])


def image_name():
    name = ''
    letters = string.ascii_letters
    for i in range(10):
        name += letters[randint(0,25)]
    return name+'.png'


def compare_images(image_1,path_1,image_2,path_2,image_3,path_3,image_4,path_4):
    speak('compairing images, have patience...')
    image_1 = imread(path_1+image_1)
    image_2 = imread(path_2+image_2)
    image_3 = imread(path_3+image_3)
    image_4 = imread(path_4+image_4)
    image_1 = cvtColor(image_1,COLOR_BGR2GRAY)
    image_2 = cvtColor(image_2,COLOR_BGR2GRAY)
    image_3 = cvtColor(image_3,COLOR_BGR2GRAY)
    image_4 = cvtColor(image_4,COLOR_BGR2GRAY)
    similarity_1 = structural_similarity(image_1,image_2)
    similarity_2 = structural_similarity(image_3,image_4)
    similarity_1 *= 100
    similarity_2 *= 100
    return round(similarity_1,2), round(similarity_2,2)

def send_score(receiver,similarity_1,similarity_2):
    speak('compaired, sending the scores on slack...')
    os.system('firefox')
    sleep(1)
    auto.hotkey('ctrl','t')
    sleep(2)
    auto.moveTo(770,364,duration=0.25)
    auto.click()
    sleep(10)
    auto.moveTo(1577,119,duration=0.25)
    auto.click()
    auto.typewrite(receiver,0.25)
    sleep(1)
    auto.moveTo(1564,189,duration=0.25)
    auto.click()
    sleep(1)
    auto.moveTo(717,993,duration=0.25)
    auto.typewrite(f'similarity of 1st screenshot is {similarity_1}% \n',0.25)
    auto.typewrite(f'and similarity of 2nd screenshot is {similarity_2}%',0.25)
    sleep(1)
    auto.press('enter')
    auto.hotkey('winleft','d')
    speak('Done...')
    return


def main():
    greet_me()
    sleep(1)
    speak('Starting the test, hold on!')
    iteration = 0
    receiver = config['receiver']['name']
    image_1 = config['compare']['name_1']
    image_2 = config['compare']['name_2']
    path_1 = config['compare']['path_1']
    path_2 = config['compare']['path_2']
    path_3 = config['screenshot']['path_1']
    path_4 = config['screenshot']['path_2']
    speak('which browser you want to use for test')
    speak('to set browser to chrome type chrome')
    speak('or to set browser to firefox type firefox in the terminal below')
    browser = input("enter the browser name:")
    config['server']['browser'] = browser
    with open('config.ini','w') as file:
        config.write(file)
    if browser == 'firefox':
        os.system('firefox')
        sleep(1)
        auto.hotkey('ctrl','t')
        auto.moveTo(424,81,duration=0.25)
        auto.click()
    elif browser == 'chrome':
        os.system('google-chrome')
        sleep(1)
        auto.hotkey('ctrl','t')
        auto.moveTo(424,52,duration=0.25)
        auto.click()
    auto.typewrite(config['server']['addr'],0.25)
    sleep(1)
    auto.press('enter')
    sleep(2)
    auto.press('f11')
    sleep(2)
    auto.moveTo(1919,991,duration=0.25)
    sleep(2)
    scrn_shot_1 = image_name()
    im = auto.screenshot()
    im.save(path_3+scrn_shot_1)
    sleep(2)
    auto.moveTo(1917,1003,duration=0.25)
    auto.click()
    scrn_shot_2 = image_name()
    im = auto.screenshot()
    im.save(path_4+scrn_shot_2)
    sleep(2)
    auto.press('f11')
    sleep(1)
    auto.hotkey('winleft','d')
    similarity_1,similarity_2 = compare_images(image_1,path_1,image_2,path_2,scrn_shot_1,path_3,scrn_shot_2,path_4)
    send_score(receiver,similarity_1,similarity_2)
    iteration += 1
    if iteration:
        os.system('wmctrl -a code')
        speak('test completed do you want to continue testing with another candidate')
        speak('say, yes or no')
        response = take_command().lower()
        if response == 'no':
            speak('terminating, bye bye')
            exit(0)
        elif response == 'yes':
            os.system('wmctrl -a code')
            sleep(1)
            speak('Enter the server address in terminal below')
            address = input('Enter the address:')
            config['server']['addr'] = address
            with open('config.ini','w') as file:
                config.write(file)
            sleep(1)
            speak('what is the name of candidate')
            name = take_command().lower()
            sleep(1)
            speak('is the name correct, if yes then say yes, else say no, to input it manually in terminal below')
            reply = take_command().lower()
            if reply == 'no':
                name = input('Enter the name of candidate:')
            config['receiver']['name'] = name
            with open('config.ini','w') as file:
                config.write(file)
            speak(f'initializing test for {name}')
            sleep(1)
            main()
        else:
            speak("didn't understand what you said, terminating, bye bye")
            exit(0)



if __name__ == "__main__":
    main()